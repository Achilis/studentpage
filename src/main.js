import Vue from 'vue'
import App from './App.vue'
// import '@fortawesome/fontawesome-free/css/all.css'

new Vue({
  el: '#app',
  render: h => h(App)
})
